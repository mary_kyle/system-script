# generate SSH key
bash /system-scripts/install/gen-ssh.sh

bash /system-scripts/config/apply-system.sh
bash /system-scripts/config/apply-user.sh

bash /system-scripts/install/system-packages.sh

# Create user template from root user
rm -rf /home/user-template
cp -R /root /home/user-template
chmod 700 /home/user-template
