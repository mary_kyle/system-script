#!/bin/bash

set -ve

GIT_BRANCH="$( cat /etc/hostname )-$( blkid -s UUID -o value /dev/mapper/root_crypt )"

if [ ! -d /etc/.git ]; then
    rm -Rf /tmp/etc_git
    git clone ssh://git@bitbucket.org/mary_kyle/linux-etc.git /tmp/etc_git
    cd /tmp/etc_git
    git checkout -b $GIT_BRANCH
    git push -u --set-upstream origin $GIT_BRANCH

    mv /tmp/etc_git/.git /etc/.git
fi

cd /etc/
git fetch
git add -A
git commit -m "Sync issued by user. $( date "+%F %T %Z" )"
git push

