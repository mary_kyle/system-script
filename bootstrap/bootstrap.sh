#/bin/bash
# This file can be obtained via: wget https://goo.gl/L2UhYF -O bootstrap.sh

LOCALE="en_US"

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --country-code) MIRROR_COUNTRY_CODE="$2"; shift ;;
        --target-drive) TARGET_DRIVE="$2"; shift ;;
        --computer-name) COMPUTER_NAME="$2"; shift ;;
        --time-zone) TIMEZONE="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

if [ -z "$MIRROR_COUNTRY_CODE" ]
then
      echo "[--country-code] needs to be set. Will download all packages from a mirror in that country. Example for Canada: [--country-code CA]"
      exit 2
else
      echo "--country-code \"$MIRROR_COUNTRY_CODE\""
fi

if [ -z "$TARGET_DRIVE" ]
then
      echo "[--target-drive] needs to be set. Will install the system to that drive. This can't be a partition and the /dev/ prefix needs to be omitted! Example: [--target-drive sda]"
      exit 2
else
      echo "--target-drive \"$TARGET_DRIVE\""
fi

if [ -z "$COMPUTER_NAME" ]
then
      echo "[--computer-name] needs to be set. The network name of this computer will be set to this. Example: [--computer-name my-desktop]"
      exit 2
else
      echo "--computer-name \"$COMPUTER_NAME\""
fi

if [ -z "$TIMEZONE" ]
then
      echo "[--time-zone] needs to be set. The timezone to set the clock to. List of all timezones can be obtained via [timedatectl list-timezones]. Example: [--time-zone Europe/Berlin]"
      exit 2
else
      echo "--time-zone \"$TIMEZONE\""
fi

set -ve

# if this fails, you have specified an invalid drive
lsblk "/dev/$TARGET_DRIVE"

 if [ $( lsblk -n "/dev/$TARGET_DRIVE" | wc -l ) -ne 1 ]; then
    # If you get here, your drive is not empty.
    echo "Make sure it's the correct drive and then zap all data on it with 'sgdisk -Z /dev/$TARGET_DRIVE'"
    exit 1
 fi

sgdisk -p "/dev/$TARGET_DRIVE"

wget "https://www.archlinux.org/mirrorlist/?country=$MIRROR_COUNTRY_CODE&ip_version=4&use_mirror_status=on" -O /etc/pacman.d/mirrorlist
sed '/#Server/s/#Server/Server/' /etc/pacman.d/mirrorlist > /etc/pacman.d/mirrorlist.gen
cp /etc/pacman.d/mirrorlist.gen /etc/pacman.d/mirrorlist

# Partitioning...
sgdisk -Z -g -n 1::500M -t 1:ef00 -n 2::1000M -n=3:: "/dev/$TARGET_DRIVE"

# extract UUIDs of newly created partitions
EFI_PARTITION=/dev/disk/by-partuuid/$( blkid -s PARTUUID -o value /dev/${TARGET_DRIVE}1 )
BOOT_PARTITION=/dev/disk/by-partuuid/$( blkid -s PARTUUID -o value /dev/${TARGET_DRIVE}2 )
ROOT_PARTITION=/dev/disk/by-partuuid/$( blkid -s PARTUUID -o value /dev/${TARGET_DRIVE}3 )

# check that we got valid block devices here
blkid "$EFI_PARTITION"
blkid "$BOOT_PARTITION"
blkid "$ROOT_PARTITION"

mkfs.fat -F32 "$EFI_PARTITION"
mkfs.ext4 "$BOOT_PARTITION"
cryptsetup luksFormat "$ROOT_PARTITION"
cryptsetup luksOpen "$ROOT_PARTITION" root_crypt
mkfs.ext4 /dev/mapper/root_crypt
mount /dev/mapper/root_crypt /mnt

mkdir /mnt/boot
mount "$BOOT_PARTITION" /mnt/boot

mkdir -p /mnt/media/efi
mount "$EFI_PARTITION" /mnt/media/efi

# Install base system
pacstrap /mnt base linux linux-firmware dhcpcd

echo "export TARGET_DRIVE=\"$TARGET_DRIVE\"" > bootstrap.params
echo "export EFI_PARTITION=\"$EFI_PARTITION\"" >> bootstrap.params
echo "export BOOT_PARTITION=\"$BOOT_PARTITION\"" >> bootstrap.params
echo "export ROOT_PARTITION=\"$ROOT_PARTITION\"" >> bootstrap.params
echo "export MIRROR_COUNTRY_CODE=\"$MIRROR_COUNTRY_CODE\"" >> bootstrap.params
echo "export DRIVE_PASSWORD=\"$DRIVE_PASSWORD\"" >> bootstrap.params
echo "export COMPUTER_NAME=\"$COMPUTER_NAME\"" >> bootstrap.params
echo "export LOCALE=\"$LOCALE\"" >> bootstrap.params
echo "export TIMEZONE=\"$TIMEZONE\"" >> bootstrap.params

wget https://bitbucket.org/mary_kyle/system-script/raw/master/bootstrap/bootstrap-chroot.sh -O bootstrap-chroot.sh

cat bootstrap.params
cp bootstrap-chroot.sh /mnt/root/
cp bootstrap.params /mnt/root/
chmod 777 /mnt/root/bootstrap-chroot.sh
arch-chroot /mnt bash -c /root/bootstrap-chroot.sh
reboot
