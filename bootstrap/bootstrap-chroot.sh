#/bin/bash
# This file can be obtained via: wget https://goo.gl/5IdDVe -O bootstrap-chroot.sh

set -ve

source /root/bootstrap.params
cat /root/bootstrap.params

echo "$COMPUTER_NAME" > /etc/hostname
ln -sf "/usr/share/zoneinfo/$TIMEZONE" /etc/localtime

sed "s/#$LOCALE/$LOCALE/" /etc/locale.gen > /etc/locale.gen.gen
cp /etc/locale.gen.gen /etc/locale.gen
locale-gen
echo "LANG=$LOCALE.UTF-8" > /etc/locale.conf

# Enable networking
systemctl enable systemd-networkd
systemctl enable dhcpcd

# Create fstab
{
    echo "UUID=$(blkid -s UUID -o value "/dev/mapper/root_crypt") / ext4 errors=remount-ro 0 1";
    echo "UUID=$(blkid -s UUID -o value "$BOOT_PARTITION") /boot ext4 defaults 0 2";
} > /etc/fstab


PACKAGES="openssh fuse pkgfile rsync gptfdisk pass bash-completion ntfs-3g vim ansible"

################### Package configuration
# DRIVERS - AMD
PACKAGES="$PACKAGES amd-ucode"
# DRIVERS - NVIDIA
PACKAGES="$PACKAGES nvidia"

# BASE DEVELOPMENT
PACKAGES="$PACKAGES base-devel git yasm autoconf automake make"
# JAVA
PACKAGES="$PACKAGES maven jre-openjdk maven jdk-openjdk openjdk-doc openjdk-src"
# CPP
PACKAGES="$PACKAGES clang gcc cmake"
# JS
PACKAGES="$PACKAGES nodejs npm"
# Python
PACKAGES="$PACKAGES python2"

pacman --noconfirm -S $PACKAGES

# Configure initramfs
sed '/^MODULES=.*$/s//MODULES=(ext4)/' /etc/mkinitcpio.conf > /tmp/mkinitcpio.conf.tmp
cp /tmp/mkinitcpio.conf.tmp /etc/mkinitcpio.conf
sed '/^HOOKS=.*$/s//HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt filesystems fsck)/' /etc/mkinitcpio.conf > /tmp/mkinitcpio.conf.tmp
cp /tmp/mkinitcpio.conf.tmp /etc/mkinitcpio.conf

mkinitcpio -p linux

# Configure GRUB
pacman --noconfirm -S grub efibootmgr

sed "s%GRUB_CMDLINE_LINUX=\".*\"%GRUB_CMDLINE_LINUX=\"cryptdevice=$ROOT_PARTITION:root_crypt\"%" /etc/default/grub > /etc/default/grub.gen
cp /etc/default/grub.gen /etc/default/grub

grub-install --target=x86_64-efi --efi-directory=/media/efi --bootloader-id=grub --recheck "/dev/$TARGET_DRIVE"
grub-mkconfig -o /boot/grub/grub.cfg
mkdir -p /media/efi/EFI/boot
cp /media/efi/EFI/grub/grubx64.efi /media/efi/EFI/boot/bootx64.efi

# checkout system scripts
if [ ! -d /system-scripts ]; then
    git clone https://mary_kyle@bitbucket.org/mary_kyle/system-script.git /system-scripts
    chmod -R 755 /system-scripts
fi
cd /system-scripts
git pull

# Set root password
passwd

rm /root/bootstrap.params
rm /root/bootstrap-chroot.sh


exit
