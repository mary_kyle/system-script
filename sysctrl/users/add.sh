#!/usr/bin/sh

set -e

echo "Enter a username: "
read new_user
echo "Creating user \'$new_user\'"

useradd -m "$new_user"
rsync --recursive /home/user-template/ "/home/$new_user/"
chown -R "$new_user" "/home/$new_user/"
echo "Enter a password for the new user:"
passwd "$new_user"
echo "User created!"
