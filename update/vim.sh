#!/usr/bin/sh

set -ve

cp /system-scripts/config/resources/vimrc ~/.vimrc

vim +PluginInstall +qall
vim +PluginUpdate +qall

bash ~/.vim/bundle/YouCompleteMe/install.sh --omnisharp-completer --clang-completer

