#!/bin/bash

set -ev
source /system-scripts/framework.include

PREFIX="$HOME/apps/node"

git_clone_cd https://github.com/nodejs/node.git "$PREFIX"
git checkout --force tags/$( git tag | sort | sed -n '/^v[0-9]\.[0-9]\.[0-9]$/p' | tail -n 1)
./configure --with-intl=full-icu --download=all "--prefix=$PREFIX/dist"
make -j4
make install

