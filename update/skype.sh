#!/bin/bash

set -v

if [ ! -d ~/apps/skype ]; then
    mkdir ~/apps/skype || exit 1

fi

cd ~/apps/skype || exit 1

wget http://www.skype.com/go/getskype-linux-beta-dynamic -O ./new-download.tar.bz2 || exit 1

cmp --silent ./new-download.tar.bz2 ./old-download.tar.bz2

if [ $? -ne 0 ]; then
    rm -rf ./dist || exit 1
    tar xf ./new-download.tar.bz2 || exit 1
    mv ./skype-* ./dist
    mv -f ./new-download.tar.bz2 ./old-download.tar.bz2
fi

exit 0

