#!/bin/bash

set -v

if [ ! -d ~/apps/hipchat ]; then
    mkdir ~/apps/hipchat || exit 1

fi

cd ~/apps/hipchat || exit 1

wget http://downloads.hipchat.com/linux/arch/hipchat-x86_64.tar.xz -O ./new-download.tar.xz || exit 1

cmp --silent ./new-download.tar.xz ./old-download.tar.xz

if [ $? -ne 0 ]; then
    rm -rf ./opt
    rm -rf ./usr
    tar xf ./new-download.tar.xz || exit 1
    mv -f ./new-download.tar.xz ./old-download.tar.xz
fi

