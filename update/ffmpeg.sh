#!/bin/bash

set -ev
source /system-scripts/framework.include

PREFIX="$HOME/apps/ffmpeg/"

git_pull_cd https://github.com/FFmpeg/FFmpeg.git "$PREFIX"
rm -R "$PREFIX/dist" || true
./configure --prefix="$PREFIX/dist" --enable-shared --extra-cflags="-fPIC"
make -j4
make install

