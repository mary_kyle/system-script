#!/bin/bash

set -v

if [ ! -d ~/apps/atom ]; then
    mkdir ~/apps/atom || exit 1

fi

cd ~/apps/atom || exit 1

wget https://atom.io/download/deb -O ./new-download.deb || exit 1

cmp --silent ./new-download.deb ./old-download.deb

if [ $? -ne 0 ]; then
    rm -rf ./usr
    ar xf ./new-download.deb || exit 1
    tar xf ./data.tar.gz || exit 1
    rm ./control.tar.gz
    rm ./data.tar.gz
    rm -rf ./debian-binary
    mv -f ./new-download.deb ./old-download.deb
fi

