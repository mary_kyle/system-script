#!/bin/bash

set -v

if [ ! -d ~/apps/xmind ]; then
    mkdir ~/apps/xmind || exit 1

fi

cd ~/apps/xmind || exit 1


LINK=$( curl -v --silent http://www.xmind.net/download/linux/ --stderr -  | sed -n '/amd64.deb/s#.*href=".*/\([a-zA-Z0-9_-\.]\+_amd64.deb\)".*#\1#p' )
wget "http://dl2.xmind.net/xmind-downloads/$LINK" -O ./new-download.deb || exit 1

cmp --silent ./new-download.deb ./old-download.deb

if [ $? -ne 0 ]; then
    rm -rf ./usr
    ar xf ./new-download.deb || exit 1
    tar xf ./data.tar.gz || exit 1
    rm ./control.tar.gz
    rm ./data.tar.gz
    rm -Rf ./debian-binary
    rm -Rf ./script
    mv -f ./new-download.deb ./old-download.deb

    sed "s#XMIND=.*#XMIND=$HOME/apps/xmind/usr/lib/xmind/XMind#" ./usr/bin/XMind > ./usr/bin/XMind.tmp
    mv -f ./usr/bin/XMind.tmp ./usr/bin/XMind
fi
