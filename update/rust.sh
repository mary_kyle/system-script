#!/usr/bin/sh

set -ve

cd ~/apps/
if [ ! -d ./rust ]; then
    git clone https://github.com/rust-lang/rust.git
    git clone https://github.com/phildawes/racer.git
    git clone https://github.com/rust-lang/cargo.git

    cd ./cargo
    git submodule update --init
    python src/etc/install-deps.py
    cd ..
else
    cd rust
    git pull

    cd ../racer
    git pull

    cd ../cargo
    git pull

    cd ..
fi

cd rust
./configure --prefix=$HOME/apps/rust/dist
make -j4
make install

cd ../cargo
./configure --local-rust-root=$HOME/apps/rust/dist --prefix=$HOME/apps/cargo/dist
make
make install

cd ../racer
cargo build --release

cp ~/apps/racer/plugin/racer.vim ~/.vim/plugin/

