#!/bin/bash

function git_clone_cd() {
    GIT_REPO="$1"
    TARGET_DIR="$2"

    if [ ! -d "$TARGET_DIR" ]; then
        git clone "$GIT_REPO" "$TARGET_DIR"
    else
        echo "Pulling from repository: $GIT_REPO"
    fi

    cd "$TARGET_DIR"
}

