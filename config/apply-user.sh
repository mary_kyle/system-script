#!/bin/bash

set -ve

CONFIG="/system-scripts/config/"

bash "$CONFIG/apply-bashrc.sh"
bash "$CONFIG/apply-vim.sh"
bash "$CONFIG/apply-X.sh"

ln -sf /system-scripts/launchers ~/launchers
