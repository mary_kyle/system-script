set -e

function setup {

    # reset and block everything that is not explicitly allowed
    $1 -P INPUT DROP
    $1 -P FORWARD DROP
    $1 -P OUTPUT DROP

    $1 -t nat -F
    $1 -t mangle -F
    $1 -F
    $1 -X

    # allow everything on localhost
    $1 -A INPUT  -i lo -j ACCEPT
    $1 -A OUTPUT -o lo -j ACCEPT

    # allow outgoing TCP & UDP
    $1 -A OUTPUT -p tcp -m state --state NEW,ESTABLISHED -j ACCEPT
    $1 -A INPUT  -p tcp -m state --state ESTABLISHED -j ACCEPT
    $1 -A OUTPUT -p udp -m state --state NEW,ESTABLISHED -j ACCEPT
    $1 -A INPUT  -p udp -m state --state ESTABLISHED -j ACCEPT

    # allow outgoing ICMP
    $1 -A OUTPUT -p icmp -m state --state NEW,ESTABLISHED -j ACCEPT
    $1 -A INPUT  -p icmp -m state --state ESTABLISHED     -j ACCEPT

    # print final configuration
    $1 -L -v -n

    iptables-save > /etc/iptables/$1.rules
}

echo "Setting up IPv4 firewall..."
setup iptables

echo "Setting up IPv6 firewall..."
setup ip6tables
