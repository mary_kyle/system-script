#!/bin/bash

set -ve

CONFIG="/system-scripts/config/"

bash "$CONFIG/apply-iptables.sh"
bash "$CONFIG/apply-sudo.sh"

# make python2 the default
ln -sf /usr/bin/python2.7 /usr/bin/python

# configure kernel modules to load
cp "$CONFIG/resources/modprobe-system.conf" /etc/modules-load.d/

# enable services
systemctl enable gdm.service
