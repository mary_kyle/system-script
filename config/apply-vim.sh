#!/usr/bin/sh

set -ve

if [ ! -d ~/.vim/bundle/Vundle.vim ]; then
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

    cp /system-scripts/config/resources/vimrc-stripped ~/.vimrc
    vim +PluginInstall +qall
fi

cp /system-scripts/config/resources/vimrc ~/.vimrc

vim +PluginInstall +qall

