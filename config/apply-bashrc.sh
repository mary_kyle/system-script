#!/bin/bash

set -ve

cp /system-scripts/config/resources/bashrc ~/.bashrc
cp /system-scripts/config/resources/profile ~/.profile

