#!/usr/bin/sh

set -ve

HAS_GROUP=$( cat /etc/group | sed -n '/sudo/p' | wc -l )
if [ $HAS_GROUP -eq 0 ]; then
    groupadd sudo
fi

