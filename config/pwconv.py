import re
from subprocess import Popen, PIPE, STDOUT

data = ""

with open ("pwsafe.txt", "r") as myfile:
    data=myfile.read()

list = {}

for line in re.split('\\n', data):
    elems = re.split('\\t', line)

    if len(elems) != 7:
        continue

    name = elems[0].replace(" ", "").replace(".", "|").replace("/", ".").replace("|", "/")
    user = elems[1]
    pwd = elems[2]
    url = elems[3]
    created = elems[4]
    email = elems[5]
    notes = elems[6]

    if(name in list):
        x = ""
        for i in range(1,100):
            x = str(i)
            if(name + x in list):
                continue
            else:
                break
        name += x

    feed = pwd + "\n" + "user: " + user + "\n" + "url: " + url + "\n" + "email: " + email + "\n" + "notes: " + notes + "\n" + "created: " + created + "\n"

    p = Popen(['pass', 'insert', '--multiline', '--force', name], stdout=PIPE, stdin=PIPE, stderr=PIPE)

    print('pass insert --multiline --force "' + name + '"')
    print(p.communicate(input=feed))
    if(p.returncode != 0):
        print("FAILED")
        exit(p.returncode)


