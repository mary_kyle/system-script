#!/bin/bash

cp /system-scripts/config/resources/Xresources ~/.Xresources
cp /system-scripts/config/resources/Xmodmap ~/.Xmodmap
cp /system-scripts/config/resources/xinitrc ~/.xinitrc
